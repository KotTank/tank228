import turtle, random
andrey = turtle.Screen()
andrey.bgcolor('#009977')
andrey.setup(600,600)

snake = []
for q in range(5):
    t = turtle.Turtle()
    t.shape('square')
    t.up()
    t.speed(0)
    snake.append(t)

andrey.onkeypress(lambda: snake[0].setheading(90), 'w')
andrey.onkeypress(lambda: snake[0].setheading(0), 'd')
andrey.onkeypress(lambda: snake[0].setheading(180), 'a')
andrey.onkeypress(lambda: snake[0].setheading(270), 's')



andrey.listen()



apple = turtle.Turtle()
apple.shape('circle')
apple.color('red')
apple.up()
apple.speed(0)



while True:
    if snake[0].distance(apple) < 20:
        x = random.randrange(-280, 280, 20)
        y = random.randrange(-280, 280, 20)
        apple.goto(x, y)
        
        t = turtle.Turtle()
        t.shape('square')
        t.up()
        t.speed(0)
        snake.append(t)





    
    snake[0].forward(20)
    for i in range(len(snake)-1, 0, -1):
        x = snake[i - 1].xcor()
        y = snake[i - 1].ycor()
        snake[i].goto(x, y)
    x = snake[0].xcor()
    y = snake[0].xcor()


    if x > 280 or x < -280 or y > 280 or y < -280:
        break
    if len(snake) > 10:
        andrey.bgcolor('009900')
        break






        
