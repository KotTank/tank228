import tkinter, random

okno = tkinter.Tk()
okno.geometry('600x600')


holst = tkinter.Canvas(okno,
                       width = 600,
                       height = 600)
holst.pack()


SIZE = 10
PIXEL = 60


button = []
game = []
for x in range (SIZE):
    stolb = []
    for y in range (SIZE):
        stolb.append(0)
        b = holst.create_rectangle(x*PIXEL, y*PIXEL,
                                   x*PIXEL + PIXEL, y*PIXEL + PIXEL,
                                   fill = '#00ff00')
        button.append(b)
    game.append(stolb)

def show_ships():
    for x in range (SIZE):
        for y in range (SIZE):
            if game [x] [y] == 1:
                nom = x * SIZE + y
                holst.itemconfig(button[nom],
                                 fill = 'black')



def set_ships(paluba):
    sdelal = False
    while not sdelal:
        x = random.randint(0, SIZE)
        y = random.randint(0, SIZE)
        print(x, y)
        if y >= paluba -1:
            x0 = x -1 if x>0 else x
            x1 = x +1 if x< SIZE-1 else SIZE - 1
            y0 = y - paluba if y - paluba-1 > 0 else y - paluba+1
            y1 = y +1 if y < SIZE-1 else y

            nelza = False
            for xx in (x0, x1):
                for yy in (y0, y1):
                    nom = y*SIZE + x
                    holst.itemconfig(button[nom],
                                            fill='#0000ff')
                    if game [xx][yy]==1:
                        sdelal = False
                        nelza = True


            if not nelza:
                sdelal = True
                for i in range(paluba):
                    game[x][y-i] = 1
            
set_ships(4)
set_ships(4)


show_ships()
        



